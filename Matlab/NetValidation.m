tic
tolerance = 0.3;

numberSimuls = 100000;
countHit = 0;

countNotHit = 0;
meanError = 0;
countErrorClose = 0;
countErrorFar = 0;

closeDist = 2.5;
farDist = 5;

wait = waitbar(0,'Starting Validation...');

for ii = 0:1:numberSimuls
    KickerAnguladoGolOlimpicoSimul
    error = min(sqrt((X1-Xobjective(1)).^2+(X2-Xobjective(2)).^2));
    
    waitbar(ii/numberSimuls,wait,strcat('Case number:',num2str(ii),' of ',num2str(numberSimuls)));
    
    if error <= tolerance
        countHit = countHit+1;
    else 
        countNotHit = countNotHit+1;
        if min(sqrt((Xini(1)-Xobjective(1)).^2+(Xini(2)-Xobjective(2)).^2)) < closeDist
            countErrorClose = countErrorClose+1;
            
        else
            if min(sqrt((Xini(1)-Xobjective(1)).^2+(Xini(2)-Xobjective(2)).^2)) > farDist
                countErrorFar = countErrorFar+1;
            end
        end
    end
    meanError = meanError+error;
end

close(wait)
meanError = meanError/numberSimuls;
totalTima = toc;

figure
pieTotalError = [countHit/numberSimuls countNotHit/numberSimuls];
labelsTotalError = {'Target Hit', 'Target Not Hit'};
p1 = pie(pieTotalError);
legend(labelsTotalError);

figure
pieError = [countErrorClose/countNotHit (countNotHit-(countErrorClose))/countNotHit];
labelsError = {'Target too close', 'Other cases'};
p2 = pie(pieError);
legend(labelsError);
