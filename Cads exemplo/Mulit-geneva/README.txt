                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1252585
Mulit-geneva by mgg942 is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Not for beginners.
This is an over-the-top geneva wheel model. Inspired by a simulation example provided in SimLab Composer Mechanical V6.1
Designed in Moment of Inspiration (MOI), simulated in SimLab Composer Mechanical and printed mostly in PLA on an Ultimaker Original.
A few smaller parts printed in ABS on a Robox.
This is not an easy print. The largest part, the Frame, is difficult to keep flat even in PLA so the use of a brim is strongly recommended. With a brim the Frame just fits on the Ultimaker after a bit of fiddling to find the right angle to set it at.

# Print Settings

Printer Brand: Ultimaker
Printer: Ultimaker Original
Supports: No
Resolution: 0.2mm
Infill: Various

Notes: 
I had difficulty making the larger pieces. I believe that mostly this was due to a worn extruder drive wheel, but it has made it a little difficult to make printing recommendations. However I'd suggest using 1.2mm wall sections and 60% fill on the Frame to ensure adequate strength in the connection of the taller posts to the base of the frame.
Elsewhere I'd suggest 1.2mm walls and 30-40% fill.
Normally I use 210C for PLA but because of the extrusion difficulties I was having I increased this to 220C.
I only made the Keeper Old and the KeeperTops on the Robox in ABS. Default fine settings.
The design evolved and the assembly that I made is NOT to the latest design. To make the same as I did use Frame Old and Keeper Old and make two Keeper Tops (which are not required with the later design). The reason that I've not made a Frame New is that my Ultimaker is out of action awaiting a new Hagen Extruder Drive Wheel.
The difference between the two designs in that in the Old design the Keeper that carries the drive parts is connected to the frame via the press fit Keeper Tops and thus is not easily removable.
The later design uses screws to connect those parts.
All screws are 3mm x 10mm long Stainless Steel Button Head Cap Screws.
A video of the 'Thing' in action is here: https://youtu.be/xYAFBgePTWM
My hand obscures a lot of what is going on in the video and the mechanism is more clearly visible in the simulation (made in SimLab Composer Mechanical) here: https://youtu.be/tYTAA3nV_Oo
For an even better view check out the 3D pdf file in which you can view the mechanism from any angle and then animate it.

# Post-Printing

I design rotating parts with the axle (pin, shaft) the same nominal size as the hole. Then I ream the hole to size using a hand reamer. (If you're using hand reamer you may be interested in these reamer handles: http://www.thingiverse.com/thing:386855).
So, clean off excess material, and ream all holes associated with rotation. Note do NOT ream the 8mm diameter hole in the keeper handle..
Ensure that the base rotates freely in the frame.
Ensure that all the 3 slot genevas rotate freely on the frame.
Ensure that the driver pin can be moved freely in the slots of the five slot geneva in the centre of the base.
Then position the base on the frame and mount the 3 slot genevas one at a time ensuring that each 3 slot geneva witll index smoothly when rotating the base by hand.
Ensure that the driver shaft rotates freely in the keeper.
Ensure that the handle stem rotates freely in the handle sleeve.
Assemble the handle stem, sleeve and keeper handle using a soft hammer.
Assemble the driver, driver shaft and keeper using 3mm x 10mm long screws.(I used 4 screws to assemble the driver and driver shaft, though there is provision for 8 screws.)
Finally secure the keeper to the frame.
Frame new has provision for washers to retain the 3 slot genevas. I suggest that these be fitted (if you wish to do so) AFTER you have ensured that all is operating smoothly.

# How I Designed This

There is one essential formula to be used when designing geneva mechanisms.  I found this link very helpful: http://gpusupercomputing.com/adarsh/Geneva.pdf

Otherwise pretty straightforward using Moment of Inspiration for the 3D design and SimLab Composer Mechanical for the simulation.